package org.tparker.challenge;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.tparker.challenge.alerts.BatteryAlert;
import org.tparker.challenge.alerts.TemperatureAlert;
import org.tparker.challenge.utils.PMCUtils;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import static org.tparker.challenge.consts.PMCConsts.*;

/**
 * PagingMissionControl - Class to analyze and process mission control data and display any warning messages which
 * may arise.
 */
public class PagingMissionControl {

    private static final String INPUT_FILE = "input.txt";

    private String inputData;
    private LocalDateTime timestamp;
    private int satelliteId;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private float rawValue;
    private String component;

    public String getInputData() {
        return inputData;
    }

    public void setInputData(String inputData) {
        this.inputData = inputData;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(int redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(int yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(int yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public float getRawValue() {
        return rawValue;
    }

    public void setRawValue(float rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    private final PMCUtils pmcUtils = new PMCUtils();
    private final TemperatureAlert temperatureAlert = new TemperatureAlert();
    private final BatteryAlert batteryAlert = new BatteryAlert();


    // Constructors
    public PagingMissionControl() {}

    public PagingMissionControl(String inputData) {
        this.inputData = inputData;
    }

    public PagingMissionControl(LocalDateTime timestamp, int satelliteId, int redHighLimit, int yellowHighLimit, int yellowLowLimit, int redLowLimit, float rawValue, String component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    /**
     * main - main class to run the PagingMissionControl class.
     * @param args - String arguments passed into the main method
     */
    public static void main(String[] args) {
        PagingMissionControl pagingMissionControl = new PagingMissionControl();
        boolean status = pagingMissionControl.findAlerts();
        System.out.println("findAlerts() returned a status of " + status);
        System.out.println("\n");
    }

    /**
     * findAlerts - Find alerts based on input data
     */
    public boolean findAlerts() {
        try {
            readInput();
            return true;
        } catch(FileNotFoundException e) {
            System.out.println("ERROR:  File not found..." + e.getMessage());
            return false;
        }
    }

    /**
     * readInput - read and process the input data
     * @throws FileNotFoundException - in the event of a missing or malformed file
     */
    private void readInput() throws FileNotFoundException {
        List<BatteryAlert> batteryAlertList = new ArrayList<>();
        List<TemperatureAlert> tempAlertList = new ArrayList<>();
        int batAlertCounter = 0;
        int tempAlertCounter = 0;

        System.out.println(BLANK);
        InputStream inputStream = PagingMissionControl.class.getClassLoader().getResourceAsStream(INPUT_FILE);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        Scanner read = new Scanner(br);

        while(read.hasNextLine()) {
            // read a line of data
            setInputData(read.nextLine());
            // parse the line of data
            parseData();

            //System.out.println("Component being checked = " + getComponent());
            //System.out.println("Red Low Limit = " +getRedLowLimit());
            if(batAlertCounter < MAX_ALERTS) { // If three BATT alerts haven't been detected yet
                if(getComponent().equals(BATT) && getRedLowLimit() == BATTERY_RED_LOW) {
                    batAlertCounter++;
                    if(batAlertCounter == MAX_ALERTS) {
                        batteryAlert.setSatelliteId(Integer.toString(getSatelliteId()));
                        batteryAlert.setSeverity(RED_LOW);
                        batteryAlert.setComponent(getComponent());
                        batteryAlert.setTimestamp(pmcUtils.timestampToString(getTimestamp()));
                        batteryAlertList.add(batteryAlert);
                    }
                }
                System.out.println("Battery Alert Counter = " + batAlertCounter);
            }


            //System.out.println("Component being checked = " + getComponent());
            //System.out.println("Red High Limit = " + getRedHighLimit());
            if(tempAlertCounter < MAX_ALERTS) { // If three TSTAT alerts haven't been detected yet
                if(getComponent().equals(TSTAT) && getRedHighLimit() == THERMOSTAT_RED_HIGH) {
                    tempAlertCounter++;
                    if (tempAlertCounter == MAX_ALERTS) {
                        temperatureAlert.setSatelliteId(Integer.toString(getSatelliteId()));
                        temperatureAlert.setSeverity(RED_HIGH);
                        temperatureAlert.setComponent(getComponent());
                        temperatureAlert.setTimestamp(pmcUtils.timestampToString(getTimestamp()));
                        tempAlertList.add(temperatureAlert);
                    }
                }
                System.out.println("Temperature Alert Counter = " + tempAlertCounter);
            }

            if(batAlertCounter == MAX_ALERTS && tempAlertCounter == MAX_ALERTS) {
                displayAlerts(batteryAlertList, tempAlertList);
                break;
            }
        }
        read.close();
    }

    /**
     * parseData - parses the data for each line read into separate fields
     */
    private void parseData() {
        List<String> dataList = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(getInputData(), PIPE);
        while (st.hasMoreTokens()) {
            dataList.add(st.nextToken());
        }
        setTimestamp(pmcUtils.convertTimestampString(dataList.get(0), TIMESTAMP_PATTERN));
        setSatelliteId(Integer.parseInt(dataList.get(1)));
        setRedHighLimit(Integer.parseInt(dataList.get(2)));
        setYellowHighLimit(Integer.parseInt(dataList.get(3)));
        setYellowLowLimit(Integer.parseInt(dataList.get(4)));
        setRedLowLimit(Integer.parseInt(dataList.get(5)));
        setRawValue(Float.valueOf(dataList.get(6)));
        setComponent(dataList.get(7));
    }

    /**
     * displayAlerts - Method to format the alert message in JSON and display the messages on the console.
     * @param batteryAlertList - List to store the BatteryAlerts
     * @param tempAlertList - List to store the TemperatureAlerts
     */
    public void displayAlerts(List<BatteryAlert> batteryAlertList, List<TemperatureAlert> tempAlertList ) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        System.out.println(BLANK);
        System.out.println("Temperature Alert: ");
        // Print temperature alert message
        for(TemperatureAlert temperatureAlerts : tempAlertList) {
            System.out.println(gson.toJson(temperatureAlerts));
        }

        System.out.println("Battery Alert: ");
        // Print battery alert message
        for(BatteryAlert batteryAlerts : batteryAlertList) {
            System.out.println(gson.toJson(batteryAlerts));
        }
    }
}
