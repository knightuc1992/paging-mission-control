package org.tparker.challenge.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.tparker.challenge.consts.PMCConsts.TIMESTAMP_PATTERN;
import static org.tparker.challenge.consts.PMCConsts.ZULU;

/**
 * PMCUtils - PMC Utility class used by PagingMissionControl
 */
public class PMCUtils {

    public PMCUtils(){}

    /**
     * convertTimestampString - Converts a timestamp string to Zulu LocalDateTime based on the pattern input
      * @param timestamp - timestamp string to convert to LocalDateTime
     * @param pattern - DateFormatter pattern
     * @return LocalDateTime
     */
    public LocalDateTime convertTimestampString(String timestamp, String pattern) {
        return LocalDateTime.parse(timestamp, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * timestampToString - converts LocalDateTime to a zulu Timestamp string
     * @param localDateTime - LocalDateTime timestamp to convert to timestamp String
     * @return Timestamp String
     */
    public String timestampToString(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN);
        return localDateTime.format(dateTimeFormatter) + ZULU;
    }
}
