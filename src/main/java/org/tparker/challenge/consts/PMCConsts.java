package org.tparker.challenge.consts;

/**
 * PMCConsts - Interface class for constants used by the PagingMissionControl class.
 */
public interface PMCConsts {
    String RED_LOW = "RED LOW";
    String RED_HIGH = "RED HIGH";
    String PIPE = "|";
    String BLANK = "";
    String ZULU = "Z";
    String TIMESTAMP_PATTERN = "yyyyMMdd HH:mm:ss.SSS";
    int BATTERY_RED_LOW = 8;
    int THERMOSTAT_RED_HIGH = 101;
    int MAX_ALERTS = 3;
    String TSTAT = "TSTAT";
    String BATT = "BATT";
}
