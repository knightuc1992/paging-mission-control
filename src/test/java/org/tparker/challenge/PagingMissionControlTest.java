package org.tparker.challenge;


import org.junit.Assert;
import org.junit.Test;
import org.tparker.challenge.alerts.BatteryAlert;
import org.tparker.challenge.alerts.TemperatureAlert;
import org.tparker.challenge.utils.PMCUtils;

import java.time.LocalDateTime;

import static org.tparker.challenge.consts.PMCConsts.TIMESTAMP_PATTERN;

public class PagingMissionControlTest {


    @Test
    public void batteryAlertTest() {
        System.out.println("\n");
        System.out.println("Running batteryAlertTest");
        BatteryAlert batteryAlert = new BatteryAlert("2001", "RED_LOW", "BATT", "20180101 23:05:05.021");

        String satelliteId = batteryAlert.getSatelliteId();
        System.out.println("satelliteId = " + satelliteId);
        Assert.assertNotNull(satelliteId);

        String severity = batteryAlert.getSeverity();
        System.out.println("severity = " + severity);
        Assert.assertNotNull(severity);

        String component = batteryAlert.getComponent();
        System.out.println("component = " + component);
        Assert.assertNotNull(component);

        String timestamp = batteryAlert.getTimestamp();
        System.out.println("timestamp = " + timestamp);
        Assert.assertNotNull(timestamp);

        String allValues = batteryAlert.toString();
        System.out.println("All values for batteryAlert = " + allValues);
        Assert.assertNotNull(allValues);
    }

    @Test
    public void temperatureAlertTest() {
        System.out.println("\n");
        System.out.println("Running temperatureAlertTest");
        TemperatureAlert temperatureAlert = new TemperatureAlert("2000", "RED_HIGH", "TSTAT", "20180101 23:04:06.017");

        String satelliteId = temperatureAlert.getSatelliteId();
        System.out.println("satelliteId = " + satelliteId);
        Assert.assertNotNull(satelliteId);

        String severity = temperatureAlert.getSeverity();
        System.out.println("severity = " + severity);
        Assert.assertNotNull(severity);

        String component = temperatureAlert.getComponent();
        System.out.println("component = " + component);
        Assert.assertNotNull(component);

        String timestamp = temperatureAlert.getTimestamp();
        System.out.println("timestamp = " + timestamp);
        Assert.assertNotNull(timestamp);

        String allValues = temperatureAlert.toString();
        System.out.println("All values for temperatureAlert = " + allValues);
        System.out.println("\n");
        Assert.assertNotNull(allValues);
    }

    @Test
    public void pmcUtilsTest() {
        System.out.println("\n");
        System.out.println("Running pmcUtilsTest");
        PMCUtils pmcUtils = new PMCUtils();
        String timestamp = "20180101 23:04:06.017";
        System.out.println("Converting timestamp " + timestamp + "to LocalDateTime...");
        LocalDateTime convertedTimestamp = pmcUtils.convertTimestampString(timestamp, TIMESTAMP_PATTERN);
        System.out.println("Timestamp converted to LocalDateTime = " + convertedTimestamp);
        Assert.assertNotNull(convertedTimestamp);
        System.out.println("Converting LocalDateTime timestamp " + convertedTimestamp + " to a zulu timestamp string...");
        String revertedTimestamp = pmcUtils.timestampToString(convertedTimestamp);
        System.out.println("LocalDateTime timestamp converted to zulu timestamp String = " + revertedTimestamp);
        Assert.assertNotNull(revertedTimestamp);
        System.out.println("\n");
    }

    @Test
    public void pagingMissionControlTest() {
        System.out.println("Running pagingMissionControlTest");
        PagingMissionControl pagingMissionControl = new PagingMissionControl();
        boolean status = pagingMissionControl.findAlerts();
        if (status) {
            System.out.println("pagingMissionControl.findAlerts() ran successfully...");
        } else {
            System.out.println("pagingMissionControl.findAlerts() encountered an error...");
        }
        System.out.println("\n");
        Assert.assertTrue(status);
    }
}
